module ApplicationHelper
  
  # when a user has logged, show the welcome text
  def welcome_text 
    str = ""  
    if current_user 
      str = "You are logged in as #{current_user.email} | " 
      str += link_to "Logout", log_out_path
    else 
      str = "#{link_to "Login", log_in_path} | " 
      str += link_to "Signup", sign_up_path 
    end 
  end
end
