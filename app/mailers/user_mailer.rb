class UserMailer < ActionMailer::Base
  #default from: "from@example.com"
  
  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :from => "test", :subject => "Registered")
  end
  
end
