class Article < ActiveRecord::Base
  
  attr_accessible :title, :body, :rating, :user_id
  
  # relations
  has_many :comments, :dependent => :destroy
  belongs_to :user
  
  # validations
  validates :title, :presence => true,
                    :uniqueness => true
                   
   def valid_country_code
                     unless (code =='USA') or (code =='FRA') or (code =='GER') 
                       self.errors[:code] << "Country Code you just entered is not valid"
                     end 
                   end
  # scope
  scope :rating_is_or_above, lambda {|rating| where("rating >= ?", rating) }
  
end
