class Country < ActiveRecord::Base
  attr_accessible :code, :name
  
  # relations
  has_many :users
  
  # validations
  validate :valid_country_code
    def valid_country_code
      unless (code =='USA') or (code =='FRA') or (code =='GER') 
        self.errors[:code] << "Country Code you just entered is not valid"
      end 
    end
  
  validates :name, :presence => true,
                   :length => {:maximum => 15, 
                               :message => "Must be between 1 and 15 characters"}

end
