class Category < ActiveRecord::Base
  
  attr_accessible :name
  
  # relations
  has_many :products, :through => :categories_products
  has_many :categories_products

end
