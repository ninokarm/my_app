class Comment < ActiveRecord::Base
  
  attr_accessible :content, :article_id
  
  # relations
  belongs_to :article
  
  # validations
  validates :content, :presence => true

end
