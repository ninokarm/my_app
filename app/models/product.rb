class Product < ActiveRecord::Base
  
  attr_accessible :name, :price, :description
  
  # relations
  has_many :categories, :through => :categories_products
  has_many :categories_products
  belongs_to :user
  
  #scope
  scope :price_more_than_1000, where("price > 1000")

end
