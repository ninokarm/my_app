class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :email, :username, :age, 
                  :birthday, :address, :country_id,
                  :password, :password_confirmation
                  
  #password
  attr_accessor :password
  before_save :encrypt_password
  validates :password, :presence => {:on => :create},
                         :confirmation => true
  validates :email, :presence => true, :uniqueness => true
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  # relations
  has_many :products
  has_many :articles
  belongs_to :country
  
  # override relations
  has_many :my_country_articles,
             :class_name => "Article" ,
             :foreign_key => "user_id" ,
             :conditions => "body like '%my country%'"
  
  # validations
 # validates :username,  #:presence => true,
                        #:uniqueness => true,
 #                       :length => {:minimum => 6, :maximum => 20, 
 #                                   :message => "Must be between 6 and 20 characters"},
 #                       :format => {:with => /\A[a-zA-Z]+\z/, 
 #                                   :message => "Only letters allowed"}
  
  validates :email,  :presence => true,
                     :uniqueness => true
  
  # instance method
  def full_address
     "#{self.address} #{self.country.name}"
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def is_admin
    if self.email == "nino@karmayana.com" 
      true
    else
      false
    end
  end
end
