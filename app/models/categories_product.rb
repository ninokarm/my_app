class CategoriesProduct < ActiveRecord::Base
  
  attr_accessible :category_id, :product_id
  
  # relations
  belongs_to :category
  belongs_to :product

end
