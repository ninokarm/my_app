class Admin::ArticlesController < Admin::ApplicationController
  
  before_filter :require_admin_login
  
  def index
    @articles = Article.all
  end
  
  def new
    @article = Article.new
  end
  
  def create
    @article = Article.new(params[:article].merge(:user_id => current_user.id))
    if @article.save
      flash[:notice] = "Article was successfully created."
      redirect_to :action => :index 
    else
      flash[:error] = "Article failed to create."
      render :action => "new"
    end
  end
   
  def edit 
    @article = Article.find_by_id(params[:id])
  end
  
  def update
    @article = Article.find_by_id(params[:id])
    if @article.update_attributes(params[:article])
      flash[:notice] = "Article was successfully updated."
      redirect_to :action => :index 
    else
      flash[:error] = "Article failed to update."
      render :action => "edit"
    end
  end
  
  def destroy 
    @article = Article.find_by_id(params[:id])
    if @article.destroy
      flash[:notice] = "Article was successfully DELETED."
      redirect_to :action => :index 
    else
      flash[:error] = "Article failed to delete."
      redirect_to :action => :index 
    end
  end
  
  def show
    @article = Article.find_by_id(params[:id])
    @comments = @article.comments
    @comment = Comment.new
  end
end
