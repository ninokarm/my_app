class CommentsController < ApplicationController
  
  # must be logged in to access comments
  before_filter :require_login
  
  def create
    @comment = Comment.new(params[:comment])#.merge(:article_id => current_article.id))  
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_path(article), :notice => 'Comment was successfully created.') }
        format.js
      end
    end
    #if @comment.save
      #flash[:notice] = "Your comment has been saved"
      #redirect_to article_path(@comment.article_id)
    #else
      #flash[:error] = "Failed to save your comment"
      #render :action => "new"
    #end
  end

end
