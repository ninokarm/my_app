class AddForeignKeyToArticle < ActiveRecord::Migration
  def change
    # A user can have many articles
    add_column :articles, :user_id, :integer
  end
end
