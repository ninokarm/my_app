class AddForeignKeyToProduct < ActiveRecord::Migration
  def change
    # A user can have many products
    add_column :products, :user_id, :integer
  end
end
