class AddForeignKeyToComment < ActiveRecord::Migration
  def change
    # An article can have many comments
    add_column :comments, :article_id, :integer
  end
end
