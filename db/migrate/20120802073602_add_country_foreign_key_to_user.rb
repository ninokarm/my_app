class AddCountryForeignKeyToUser < ActiveRecord::Migration
  def change
    # A user has one country
    # A country can have many users
    add_column :users, :country_id, :integer
  end
end
