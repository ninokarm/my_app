class RenameUserNametoUsernameOnUsers < ActiveRecord::Migration
  def up
    rename_column :users, :user_name, :username
  end

end
