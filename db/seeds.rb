# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([ {first_name: 'Martin', last_name: 'Fox', email: 'fox@fox.com', 
                       username: 'martin.fox', address: '123 Fox Street', age: '23', birthday: '01-12-1974'}, 
                      
                      {first_name: 'John', last_name: 'Doe', email: 'doe@doe.com', 
                       username: 'john.doe', address: '123 Doe Street', age: '32', birthday: '12-12-1944'},
                       
                      {first_name: 'Jane', last_name: 'Smith', email: 'smith@smith.com', 
                      username: 'jane.smith', address: '123 Smith Street', age: '24', birthday: '23-12-1982'},
                      
                      {first_name: 'Diana', last_name: 'Black', email: 'black@black.com', 
                       username: 'diana.black', address: '123 Black Street', age: '31', birthday: '12-09-1976'},
                      
                      {first_name: 'Matt', last_name: 'White', email: 'white@white.com', 
                       username: 'matt.white', address: '456 White Street', age: '40', birthday: '31-11-1984'},
                       
                      {first_name: 'Stephanie', last_name: 'Small', email: 'small@small.com', 
                       username: 'steph.small', address: '678 Small Road', age: '36', birthday: '09-10-1976'}   
                    ])

countries = Country.create([ {code: 'INA', name: 'Indonesia'},
                             {code: 'AUS', name: 'Australia'},
                             {code: 'USA', name: 'USA'},
                             {code: 'ENG', name: 'England'},
                             {code: 'JPN', name: 'Japan'}
                          ])
                        
articles = Article.create([ {title: 'Article One', body: 'Lorem Ipsum One'},
                            {title: 'Article Two', body: 'Lorem Ipsum Two'},
                            {title: 'Article Three', body: 'Lorem Ipsum Three'},
                            {title: 'Article Four', body: 'Lorem Ipsum Four'},
                            {title: 'Article Five', body: 'Lorem Ipsum Five'}
                         ])
                         
comments = Comment.create([ {content: 'Comment Number One'},
                            {content: 'Comment Number Two'},
                            {content: 'Comment Number Three'},
                            {content: 'Comment Number Four'},
                            {content: 'Comment Number Five'}  
                         ])